package no.solveagent.core.agent.tansformer;

import net.bytebuddy.agent.builder.AgentBuilder;
import net.bytebuddy.asm.AsmVisitorWrapper;
import net.bytebuddy.asm.AsmVisitorWrapper.ForDeclaredMethods;
import net.bytebuddy.description.type.TypeDescription;
import net.bytebuddy.dynamic.DynamicType.Builder;
import net.bytebuddy.matcher.ElementMatcher;
import net.bytebuddy.utility.JavaModule;

public abstract class SolveAgentTransformer {

	public AgentBuilder.Transformer getTransformer() {
		final AsmVisitorWrapper.ForDeclaredMethods advice = getAdvice();

		return new AgentBuilder.Transformer() {

			@Override
			public Builder<?> transform(Builder<?> builder, TypeDescription typeDescription, ClassLoader classLoader,
					JavaModule module) {
				return builder.visit(advice);
			}
		};
	}
	
	public abstract ForDeclaredMethods getAdvice();
	public abstract ElementMatcher<? super TypeDescription> getType();
}
