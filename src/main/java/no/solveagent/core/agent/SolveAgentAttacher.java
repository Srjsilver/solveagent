package no.solveagent.core.agent;

import java.lang.instrument.Instrumentation;
import java.util.ArrayList;
import java.util.List;

import net.bytebuddy.agent.builder.AgentBuilder;
import no.solveagent.core.agent.tansformer.SolveAgentTransformer;
import no.solveagent.plugins.spring.web.SpringWebControllerTransformer;
import no.solveagent.plugins.spring.web.SpringWebDispatcherServletTransformer;
import no.solveagent.plugins.sql.connection.SqlConnectionTransformer;
import no.solveagent.plugins.sql.preparestatement.SqlPrepareStatementCloseTransformer;
import no.solveagent.plugins.sql.preparestatement.SqlPrepareStatementExecuteTransformer;
import no.solveagent.plugins.sql.preparestatement.SqlPrepareStatementSettersTransformer;
import no.solveagent.plugins.sql.preparestatement.SqlPreparedStatementExecuteQueryTransformer;
import no.solveagent.plugins.sql.preparestatement.SqlPreparedStatementGetMetaDataTransformer;

public class SolveAgentAttacher {

	
	private static AgentBuilder initiateBuilderRetransformation() {
		return new AgentBuilder.Default()
				.with(AgentBuilder.RedefinitionStrategy.RETRANSFORMATION)
				.disableClassFormatChanges();
	}
	private static List<SolveAgentTransformer> initBytebuddyTransformers(Instrumentation instr) {
		List<SolveAgentTransformer> transformers = new ArrayList<>();
		//spring
		transformers.add(new SpringWebControllerTransformer());
		transformers.add(new SpringWebDispatcherServletTransformer());
		//sql
		transformers.add(new SqlConnectionTransformer());
		transformers.add(new SqlPrepareStatementSettersTransformer());
		transformers.add(new SqlPrepareStatementExecuteTransformer());
		transformers.add(new SqlPreparedStatementExecuteQueryTransformer());
		transformers.add(new SqlPreparedStatementGetMetaDataTransformer());
		transformers.add(new SqlPrepareStatementCloseTransformer());
		return transformers;
	}
	
	private static AgentBuilder attachTransformers(Instrumentation instr) {
		AgentBuilder agentBuilder = initiateBuilderRetransformation();
		List<SolveAgentTransformer> transformers = initBytebuddyTransformers(instr);
		for(SolveAgentTransformer transformer: transformers) {
			agentBuilder = agentBuilder
					.type(transformer.getType())
					.transform(transformer.getTransformer())
					.asDecorator();
		}
		return agentBuilder;
	}
	public static void installAgent(Instrumentation instr) {
		attachTransformers(instr).installOn(instr);
	}
}
