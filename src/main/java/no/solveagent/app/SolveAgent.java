package no.solveagent.app;

import java.lang.instrument.Instrumentation;

import no.solveagent.core.agent.SolveAgentAttacher;
import no.solveagent.utils.Banner;

public class SolveAgent 
{
	
	public static void premain(String agentArgs, Instrumentation instr){
		Banner.print();
		SolveAgentAttacher.installAgent(instr);
	}
	
	public static void agentmain(String agentArgs, Instrumentation instr) {
		premain(agentArgs, instr);
	}
}
