package no.solveagent.plugins.jms;

import java.lang.reflect.Method;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.bytebuddy.asm.Advice;

public class JmsListenerInterceptor {
	@Advice.OnMethodExit
	public static void intercept(@Advice.AllArguments Object[] allArguments,
			@Advice.Origin Method method) {
		
		Logger logger = LoggerFactory.getLogger(method.getDeclaringClass());
		logger.info("Method {} of class {} called", method.getName(), method
				.getDeclaringClass().getSimpleName());

		for (Object argument : allArguments) {
			logger.info("Method {}, parameter type {}, value={}",
					method.getName(), argument.getClass().getSimpleName(),
					argument.toString());
		}
	}
}
