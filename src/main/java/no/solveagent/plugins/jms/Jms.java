package no.solveagent.plugins.jms;

import java.lang.instrument.Instrumentation;

import net.bytebuddy.agent.builder.AgentBuilder;
import net.bytebuddy.asm.Advice;
import net.bytebuddy.matcher.ElementMatchers;

public class Jms {

	private static final String messageConsumer = "javax.jms.MessageConsumer";
	private static final String messageSender = "javax.jms.MessageProducer";

	public static void transform(Instrumentation instr) {
		install(messageConsumer, "onMessage", instr);
		install(messageSender, "send", instr);
	}
	
	private static void install(
			String className, 
			String methoName, 
			Instrumentation instr
			) {
    	new AgentBuilder.Default().disableClassFormatChanges()
    	.with(AgentBuilder.RedefinitionStrategy.RETRANSFORMATION)
    	.type(ElementMatchers.named(className))
    	.transform((builder, typeDescription, classLoader, module) -> {
    		return builder.visit(Advice.to(JmsSendInterceptor.class).on(ElementMatchers.named(methoName)));
    	}).installOn(instr);
    }
}
