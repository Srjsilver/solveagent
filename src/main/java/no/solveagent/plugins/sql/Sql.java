package no.solveagent.plugins.sql;

import java.lang.instrument.Instrumentation;

import net.bytebuddy.agent.builder.AgentBuilder;
import net.bytebuddy.agent.builder.AgentBuilder.Listener;
import net.bytebuddy.asm.Advice;
import net.bytebuddy.description.method.MethodDescription;
import net.bytebuddy.dynamic.DynamicType.Builder;
import net.bytebuddy.matcher.ElementMatchers;
import no.solveagent.plugins.sql.Interceptor.InterceptSqlConnectionPreparedStatement;
import no.solveagent.plugins.sql.Interceptor.InterceptSqlExecute;
import no.solveagent.plugins.sql.Interceptor.InterceptSqlExecuteQuery;
import no.solveagent.plugins.sql.preparestatement.interceptor.InterceptPrepareStatementClose;
import no.solveagent.plugins.sql.preparestatement.interceptor.InterceptSqlPreparedStatementSetters;

@Deprecated
public class Sql {

	private static final String sqlConnection = "java.sql.Connection";
	private static final String sqlPrepareStatement = "java.sql.PreparedStatement";
	private static final String sqlStatement = "java.sql.Statement";
	private static final String ignoreSun = "com.sun";

	/*
	 * Much more to come, see: https://github.com/glowroot/glowroot/tree/
	 * 9390fcf9e364fe525e08a0e4a5516d55f0a86509/agent/plugins/jdbc-plugin/src/main/
	 * java/org/glowroot/agent/plugin/jdbc
	 */
	public static void transform(Instrumentation instr) {
		//installSqlConnectionPrepareStatementAndStatement(instr);
		//installStatementSetters(instr);	
		installPrepareStatementAndStatementExecute(instr);
		installPrepareStatementAndStatementExecuteQuery(instr);
		installPreparedStatementAndStatementClose(instr);
	}

	private static void installPreparedStatementAndStatementClose(Instrumentation instr) {
		prepareStatementAndStatementCloseAdvice(sqlPrepareStatement, "close", instr);
		
	}

	private static void prepareStatementAndStatementCloseAdvice(String className, String methodName,
			Instrumentation instr) {
		new AgentBuilder.Default()
		.disableClassFormatChanges()
		.with(AgentBuilder.RedefinitionStrategy.RETRANSFORMATION)
		.type(ElementMatchers.hasSuperType(ElementMatchers.named(className)))
		.transform((builder, typeDescription, classLoader, module) -> {
			return builder.visit(Advice.to(InterceptPrepareStatementClose.class)
					.on(ElementMatchers.named(methodName)));
		}).installOn(instr);
	}

	private static void installSqlConnectionPrepareStatementAndStatement(Instrumentation instr) {
		connectionAdvice(sqlConnection, "prepareStatement", instr);
		connectionAdvice(sqlConnection, "statement", instr);
	}

	private static void installPrepareStatementAndStatementExecuteQuery(Instrumentation instr) {
		installSqlPreparedStatementExecuteQueryAdvice(sqlPrepareStatement, "executeQuery", instr);
		installSqlPreparedStatementExecuteQueryAdvice(sqlStatement, "executeQuery", instr);
	}

	private static void installPrepareStatementAndStatementExecute(Instrumentation instr) {
		installSqlPreparedStatementExecuteAdvice(sqlPrepareStatement, "execute", instr);
		installSqlPreparedStatementExecuteAdvice(sqlStatement, "execute", instr);

	}

	private static void installStatementSetters(Instrumentation instr) {
			preparedStatementSettersAdvice(sqlPrepareStatement, instr);
			preparedStatementSettersAdvice(sqlStatement ,instr);
	}
	private static void connectionAdvice(String className, String methodName,
			Instrumentation instr) {
		new AgentBuilder.Default()
		.disableClassFormatChanges()
		.with(AgentBuilder.RedefinitionStrategy.RETRANSFORMATION)
		//.with(Listener.StreamWriting.toSystemOut())
		.type(ElementMatchers.hasSuperType(ElementMatchers.named(className)))
		.transform((builder, typeDescription, classLoader, module) -> {
			return builder.visit(Advice.to(InterceptSqlConnectionPreparedStatement.class)
					.on(ElementMatchers.named(methodName)));
		}).installOn(instr);
	}



	private static void preparedStatementSettersAdvice(String className, Instrumentation instr) {
		new AgentBuilder.Default().disableClassFormatChanges()
		.ignore(ElementMatchers.nameStartsWith(ignoreSun))
		.with(AgentBuilder.RedefinitionStrategy.RETRANSFORMATION)
		//.with(Listener.StreamWriting.toSystemError())
		.type(ElementMatchers.hasSuperType(ElementMatchers.named(className)))
		.transform((builder, typeDescription, classLoader, module) -> {
			return builder.visit(Advice.to(InterceptSqlPreparedStatementSetters.class)
					.on(ElementMatchers.named("setObject")
							.or(ElementMatchers.named("setArray"))
							.or(ElementMatchers.named("setBigDecimal"))
							.or(ElementMatchers.named("setBoolean"))
							.or(ElementMatchers.named("setByte"))
							.or(ElementMatchers.named("setDate"))
							.or(ElementMatchers.named("setDouble"))
							.or(ElementMatchers.named("setFloat"))
							.or(ElementMatchers.named("setInt"))
							.or(ElementMatchers.named("setLong"))
							.or(ElementMatchers.named("setNString"))
							.or(ElementMatchers.named("setRef"))
							.or(ElementMatchers.named("setRowId"))
							.or(ElementMatchers.named("setShort"))
							.or(ElementMatchers.named("setString"))
							.or(ElementMatchers.named("setTime"))
							.or(ElementMatchers.named("setTimestamp"))
							.or(ElementMatchers.named("setURL"))
							));
		}).installOn(instr);
	}

	private static void installSqlPreparedStatementExecuteQueryAdvice(String className, String methodName,
			Instrumentation instr) {
		new AgentBuilder.Default().disableClassFormatChanges().ignore(ElementMatchers.nameStartsWith(ignoreSun))
		.with(AgentBuilder.RedefinitionStrategy.RETRANSFORMATION)
		.type(ElementMatchers.hasSuperType(ElementMatchers.named(className)))
		.transform((builder, typeDescription, classLoader, module) -> {
			return builder
					.visit(Advice.to(InterceptSqlExecuteQuery.class).on(ElementMatchers.named(methodName)));
		}).installOn(instr);

	}

	private static void installSqlPreparedStatementExecuteAdvice(String className, String methodName,
			Instrumentation instr) {
		new AgentBuilder.Default().disableClassFormatChanges().ignore(ElementMatchers.nameStartsWith(ignoreSun))
		.with(AgentBuilder.RedefinitionStrategy.RETRANSFORMATION)
		.type(ElementMatchers.hasSuperType(ElementMatchers.named(className)))
		.transform((builder, typeDescription, classLoader, module) -> {
			return builder.visit(Advice.to(InterceptSqlExecute.class).on(ElementMatchers.named(methodName)));
		}).installOn(instr);

	}
}
