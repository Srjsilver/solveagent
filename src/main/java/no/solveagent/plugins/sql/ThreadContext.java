package no.solveagent.plugins.sql;

public class ThreadContext {
	
	private Long runningThread;
	private Object monitoredObject;
	private Long beforeTimer;
	
	public Long getRunningThread() {
		return runningThread;
	}
	public void setRunningThread(Long runningThread) {
		this.runningThread = runningThread;
	}
	public Object getMonitoredObject() {
		return monitoredObject;
	}
	public void setMonitoredObject(Object monitoredObject) {
		this.monitoredObject = monitoredObject;
	}
	public Long getBeforeTimer() {
		return beforeTimer;
	}
	public void setBeforeTimer(Long beforeTimer) {
		this.beforeTimer = beforeTimer;
	}
	public int compareTo(ThreadContext tx) {
		if(tx.getMonitoredObject() == this.monitoredObject
				&& tx.getRunningThread() == this.getRunningThread()) {
			return 0;
		}else {
			return -1;
		}
	}
}
