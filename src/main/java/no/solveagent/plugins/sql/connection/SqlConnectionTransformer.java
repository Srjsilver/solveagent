package no.solveagent.plugins.sql.connection;


import net.bytebuddy.asm.Advice;
import net.bytebuddy.asm.AsmVisitorWrapper.ForDeclaredMethods;
import net.bytebuddy.description.type.TypeDescription;
import net.bytebuddy.matcher.ElementMatcher;
import net.bytebuddy.matcher.ElementMatchers;
import no.solveagent.core.agent.tansformer.SolveAgentTransformer;
import no.solveagent.plugins.sql.connection.interceptor.InterceptConnectionPrepareStatement;

public class SqlConnectionTransformer extends SolveAgentTransformer{
	
	private static final String sqlConnection = "java.sql.Connection";
	private static final String prepareStatement = "prepareStatement";
	
	@Override
	public ForDeclaredMethods getAdvice() {
		return Advice.to(InterceptConnectionPrepareStatement.class)
		.on(ElementMatchers.named(prepareStatement));
	}

	@Override
	public ElementMatcher<? super TypeDescription> getType() {
		return ElementMatchers.hasSuperType(ElementMatchers.named(sqlConnection));
	}

}
