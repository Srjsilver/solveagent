package no.solveagent.plugins.sql.connection.interceptor;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import net.bytebuddy.asm.Advice;
import no.solveagent.plugins.sql.monitor.SqlPrepareStatementAssembler;
import no.solveagent.plugins.sql.monitor.SqlPrepareStatementWrapper;

public class InterceptConnectionPrepareStatement {
	
	@Advice.OnMethodExit
	public static void interceptAfter(
			@Advice.Return PreparedStatement prepareStatement,
			@Advice.Argument(0) String sql,
			@Advice.This Connection con,
			@Advice.Origin Method method){
		SqlPrepareStatementAssembler assembler = SqlPrepareStatementWrapper.getOrAdd(Thread.currentThread().getId());
		assembler.addSql(sql);
		try {
			assembler.addDatabase(con.getMetaData().getURL());
		} catch (SQLException e) {
			//nothing
		}
	}
}
