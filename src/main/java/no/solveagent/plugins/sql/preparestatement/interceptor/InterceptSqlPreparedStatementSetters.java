package no.solveagent.plugins.sql.preparestatement.interceptor;

import net.bytebuddy.asm.Advice;
import no.solveagent.plugins.sql.monitor.SqlPrepareStatementAssembler;
import no.solveagent.plugins.sql.monitor.SqlPrepareStatementWrapper;

public class InterceptSqlPreparedStatementSetters {

	@Advice.OnMethodExit
	public static void interceptNoReturn(
			@Advice.Argument(0) Integer paramIndex,
			@Advice.Argument(1) Object paramValue) {
		
		SqlPrepareStatementAssembler assembler = SqlPrepareStatementWrapper.getOrAdd(Thread.currentThread().getId());
		assembler.addParam(paramIndex-1, paramValue);
	}
	
}
