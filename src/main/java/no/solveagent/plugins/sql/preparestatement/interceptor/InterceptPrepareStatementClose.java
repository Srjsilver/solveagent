package no.solveagent.plugins.sql.preparestatement.interceptor;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Method;
import java.time.LocalDateTime;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.bytebuddy.asm.Advice;
import no.solveagent.plugins.sql.monitor.SqlPrepareStatementAssembler;
import no.solveagent.plugins.sql.monitor.SqlPrepareStatementWrapper;

public class InterceptPrepareStatementClose {
	
	@Advice.OnMethodEnter
	public static void interceptBefore(
			@Advice.This Object obj,
			@Advice.Origin Method method){
		
		SqlPrepareStatementAssembler assembler = SqlPrepareStatementWrapper.getOrAdd(Thread.currentThread().getId());
		String sql = assembler.assembleParamsAndSql();
		String database = assembler.getDatabase();
		String result = assembler.getResultFromExecute();
		SqlPrepareStatementWrapper.destroySqlPrepareStatementAssembler(Thread.currentThread().getId());
		try {
			File fileName = new File("Sql.txt");
			fileName.createNewFile();
			FileOutputStream oFile = new FileOutputStream(fileName, true);
			String fileOutput = LocalDateTime.now().toString() + ";";
			fileOutput += database + ";";
			fileOutput += "" + sql + ";";
			fileOutput += "" + "\"" + result + "\"" + '\n';
			oFile.write(fileOutput.getBytes());
			oFile.close();
		} catch (Exception e) {
			// nothing
		}
	}
}
