package no.solveagent.plugins.sql.preparestatement;

import net.bytebuddy.asm.Advice;
import net.bytebuddy.asm.AsmVisitorWrapper.ForDeclaredMethods;
import net.bytebuddy.description.type.TypeDescription;
import net.bytebuddy.matcher.ElementMatcher;
import net.bytebuddy.matcher.ElementMatchers;
import no.solveagent.core.agent.tansformer.SolveAgentTransformer;
import no.solveagent.plugins.sql.preparestatement.interceptor.InterceptSqlPreparedStatementSetters;

public class SqlPrepareStatementSettersTransformer extends SolveAgentTransformer{
	
	private static final String sqlPrepareStatement = "java.sql.PreparedStatement";

	
	@Override
	public ForDeclaredMethods getAdvice() {
		return Advice.to(InterceptSqlPreparedStatementSetters.class)
		.on(ElementMatchers.named("setObject")
				.or(ElementMatchers.named("setArray"))
				.or(ElementMatchers.named("setBigDecimal"))
				.or(ElementMatchers.named("setBoolean"))
				.or(ElementMatchers.named("setByte"))
				.or(ElementMatchers.named("setDate"))
				.or(ElementMatchers.named("setDouble"))
				.or(ElementMatchers.named("setFloat"))
				.or(ElementMatchers.named("setInt"))
				.or(ElementMatchers.named("setLong"))
				.or(ElementMatchers.named("setNString"))
				.or(ElementMatchers.named("setRef"))
				.or(ElementMatchers.named("setRowId"))
				.or(ElementMatchers.named("setShort"))
				.or(ElementMatchers.named("setString"))
				.or(ElementMatchers.named("setTime"))
				.or(ElementMatchers.named("setTimestamp"))
				.or(ElementMatchers.named("setURL"))
				);
	}

	@Override
	public ElementMatcher<? super TypeDescription> getType() {
		return ElementMatchers.hasSuperType(ElementMatchers.named(sqlPrepareStatement));
	}
	
}
