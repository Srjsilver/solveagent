package no.solveagent.plugins.sql.preparestatement;

import net.bytebuddy.asm.Advice;
import net.bytebuddy.asm.AsmVisitorWrapper.ForDeclaredMethods;
import net.bytebuddy.description.type.TypeDescription;
import net.bytebuddy.matcher.ElementMatcher;
import net.bytebuddy.matcher.ElementMatchers;
import no.solveagent.core.agent.tansformer.SolveAgentTransformer;
import no.solveagent.plugins.sql.preparestatement.interceptor.InterceptPreparedStatementExecuteQuery;
import no.solveagent.plugins.sql.preparestatement.interceptor.InterceptPreparedStatementGetMetaData;

public class SqlPreparedStatementGetMetaDataTransformer extends SolveAgentTransformer {

	private static final String getMetaData = "getMetaData";
	private static final String sqlPrepareStatement = "java.sql.PreparedStatement";
	
	@Override
	public ForDeclaredMethods getAdvice() {
		return Advice.to(InterceptPreparedStatementGetMetaData.class)
				.on(ElementMatchers.named(getMetaData));
	}

	@Override
	public ElementMatcher<? super TypeDescription> getType() {
		return ElementMatchers.hasSuperType(ElementMatchers.named(sqlPrepareStatement));
	}

}
