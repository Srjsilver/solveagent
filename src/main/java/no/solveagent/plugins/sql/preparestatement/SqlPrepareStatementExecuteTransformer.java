package no.solveagent.plugins.sql.preparestatement;

import net.bytebuddy.asm.Advice;
import net.bytebuddy.asm.AsmVisitorWrapper.ForDeclaredMethods;
import net.bytebuddy.description.type.TypeDescription;
import net.bytebuddy.matcher.ElementMatcher;
import net.bytebuddy.matcher.ElementMatchers;
import no.solveagent.core.agent.tansformer.SolveAgentTransformer;
import no.solveagent.plugins.sql.preparestatement.interceptor.InterceptSqlPreparedStatementExecute;

public class SqlPrepareStatementExecuteTransformer extends SolveAgentTransformer{

	private static final String sqlPrepareStatement = "java.sql.PreparedStatement";
	private static final String execute = "execute";
	
	@Override
	public ForDeclaredMethods getAdvice() {
		return Advice.to(InterceptSqlPreparedStatementExecute.class)
		.on(ElementMatchers.named(execute));
	}

	@Override
	public ElementMatcher<? super TypeDescription> getType() {
		return ElementMatchers.hasSuperType(ElementMatchers.named(sqlPrepareStatement));
	}

}
