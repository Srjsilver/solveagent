package no.solveagent.plugins.sql.preparestatement;

import net.bytebuddy.asm.Advice;
import net.bytebuddy.asm.AsmVisitorWrapper.ForDeclaredMethods;
import net.bytebuddy.description.type.TypeDescription;
import net.bytebuddy.matcher.ElementMatcher;
import net.bytebuddy.matcher.ElementMatchers;
import no.solveagent.core.agent.tansformer.SolveAgentTransformer;
import no.solveagent.plugins.sql.preparestatement.interceptor.InterceptPreparedStatementExecuteQuery;

public class SqlPreparedStatementExecuteQueryTransformer extends SolveAgentTransformer {

	private static final String executeQuery = "executeQuery";
	private static final String sqlPrepareStatement = "java.sql.PreparedStatement";
	
	@Override
	public ForDeclaredMethods getAdvice() {
		return Advice.to(InterceptPreparedStatementExecuteQuery.class)
				.on(ElementMatchers.named(executeQuery));
	}

	@Override
	public ElementMatcher<? super TypeDescription> getType() {
		return ElementMatchers.hasSuperType(ElementMatchers.named(sqlPrepareStatement));
	}
	
}
