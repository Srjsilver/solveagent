package no.solveagent.plugins.sql.preparestatement.interceptor;

import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import net.bytebuddy.asm.Advice;
import no.solveagent.plugins.sql.monitor.SqlPrepareStatementAssembler;
import no.solveagent.plugins.sql.monitor.SqlPrepareStatementWrapper;

public class InterceptPreparedStatementExecuteQuery {

	@Advice.OnMethodExit
	public static void interceptBefore(
			@Advice.This PreparedStatement obj,
			@Advice.Origin Method method,
			@Advice.Return ResultSet returnVal){
		
		SqlPrepareStatementAssembler assembler = SqlPrepareStatementWrapper.getOrAdd(Thread.currentThread().getId());
		assembler.setResult(returnVal);
	}
	
}
