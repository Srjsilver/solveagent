package no.solveagent.plugins.sql.monitor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SqlPrepareStatementWrapper {

	private static List<SqlPrepareStatementAssembler> sqlPrepareStatementAssemblerList = null;

	static {
		sqlPrepareStatementAssemblerList = new ArrayList<>();
	}

	public static synchronized SqlPrepareStatementAssembler getOrAdd(Long thread) {
		for (SqlPrepareStatementAssembler elem : sqlPrepareStatementAssemblerList) {
			if (elem.getThread() == thread) {
				return elem;
			}
		}
		SqlPrepareStatementAssembler assembler = SqlPrepareStatementAssembler.create(thread);
		sqlPrepareStatementAssemblerList.add(assembler);
		return assembler;
	}

	public static synchronized void destroySqlPrepareStatementAssembler(Long thread) {
		for (Iterator<SqlPrepareStatementAssembler> it = sqlPrepareStatementAssemblerList.iterator(); it.hasNext();) {
			SqlPrepareStatementAssembler elem = it.next();
			if (thread == elem.getThread()) {
				it.remove();
				return;
			}
		}
	}

}
