package no.solveagent.plugins.sql.monitor;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

public class SqlPrepareStatementAssembler {

	private String dataBase = null;
	private String prepareStatementSql = null;
	private Boolean execute = null;
	private Map<Integer, Object> prepareParams = null;
	private Long thread = null;
	private String result = null;

	public SqlPrepareStatementAssembler(Long thread) {
		this.thread = thread;
		prepareParams = new HashMap<>();
	}

	public synchronized void addSql(String sql) {
		this.prepareStatementSql = sql;
	}

	public synchronized void addDatabase(String dataBase) {
		this.dataBase = dataBase;
	}

	public synchronized void addParam(Integer paramIndex, Object paramObject) {
		Object paramValue = prepareParams.get(paramIndex);
		if (paramValue == null) {
			prepareParams.put(paramIndex, paramObject);
		} else {
			if (!isPrimitive(paramValue) && isPrimitive(paramObject)) {
				prepareParams.replace(paramIndex, paramObject);
			}
		}
	}

	public synchronized String assembleParamsAndSql() {
		Integer questionMarkCount = StringUtils.countMatches(prepareStatementSql, "?");
		for (Integer paramIndex = 0; paramIndex < questionMarkCount; paramIndex++) {
			prepareStatementSql = StringUtils.replaceOnce(prepareStatementSql, "?",
					String.valueOf(prepareParams.get(paramIndex)));
		}
		return prepareStatementSql;
	}

	public synchronized String getDatabase() {
		return this.dataBase;
	}

	private boolean isPrimitive(Object paramValue) {
		return paramValue instanceof String || paramValue instanceof Integer || paramValue instanceof Short
				|| paramValue instanceof Double || paramValue instanceof Float || paramValue instanceof BigDecimal
				|| paramValue instanceof Boolean;
	}

	public synchronized Long getThread() {
		return thread;
	}

	public static SqlPrepareStatementAssembler create(Long thread) {
		return new SqlPrepareStatementAssembler(thread);
	}

	public void setExecute(boolean execute) {
		this.execute = execute;
	}

	public void setResult(ResultSet result) {
		ResultSet resultSet = (ResultSet) result;
		ResultSetMetaData rsmd;
		String retVal = "";
		try {
			rsmd = resultSet.getMetaData();

			int columnsNumber;
				columnsNumber = rsmd.getColumnCount();
			
			while (resultSet.next()) {
				for (int i = 1; i <= columnsNumber; i++) {
					if (i > 1)
						retVal += (",  ");
					retVal += resultSet.getString(i) + " " + rsmd.getColumnName(i);
				}
				retVal += "\n";

			}
			
			resultSet.absolute(0);
			this.result = retVal;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		this.result = retVal;
	}

	public String getResultFromExecute() {
		if (execute != null) {
			return String.valueOf(execute);
		} else if (result != null) {
			return getResult();
		}
		return "No result was recognized";
	}

	public String getResult() {
		return this.result;
	}
}
