package no.solveagent.plugins.sql;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

public class JdbcMonitor {

	private static List<ThreadContext> threadContextList = null;
	
	static {
		threadContextList = new ArrayList<>();
	}
	public static ThreadContext get(ThreadContext threadContext) {
		for(ThreadContext thr : threadContextList) {
			if(thr.getMonitoredObject().equals(threadContext.getMonitoredObject())
					&&
			   thr.getRunningThread() == threadContext.getRunningThread()) {
				return thr;
			}
		}
		return null;
	}
	public static ThreadContext get(Object monitoredObject, Long threadId) {
		for(ThreadContext thr : threadContextList) {
			if(thr.getMonitoredObject().equals(monitoredObject)
					&&
			   thr.getRunningThread() == threadId) {
				return thr;
			}
		}
		return null;
		
	}
	public static void add(ThreadContext threadContext) {
		threadContextList.add(threadContext);
	}
	
	public static List<ThreadContext> getThreadContextList() {
		return threadContextList;
	}
	
	public static synchronized void flush(Object monitoredObject, Long threadId) {
		for(ThreadContext thr : threadContextList) {
			if(thr.getMonitoredObject().equals(monitoredObject)
					&&
			   thr.getRunningThread() == threadId) {
				threadContextList.remove(thr);
				return;
			}
		}
	}
	public static synchronized String asSql(PreparedStatement stmt) {
		
		return null;
	}

}
