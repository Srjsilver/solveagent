package no.solveagent.plugins.sql.Interceptor;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.bytebuddy.asm.Advice;

public class InterceptSqlExecuteQuery {

	@Advice.OnMethodExit
	public static void interceptAfter(
			@Advice.This Object thisObject,
			@Advice.Return Object returnObject,
			@Advice.Origin Method method) {

		Logger logger = LoggerFactory.getLogger(method.getDeclaringClass());
		File yourFile = new File("Sql.txt");
		try {
			yourFile.createNewFile();
			String fileOutput = LocalDateTime.now().toString();
			FileOutputStream oFile = new FileOutputStream(yourFile, true);
			if(returnObject != null && returnObject instanceof ResultSet) {
				ResultSet resultSet = (ResultSet) returnObject;
				ResultSetMetaData rsmd;
				String retVal = "";
				try {
					rsmd = resultSet.getMetaData();
				
				int columnsNumber = rsmd.getColumnCount();
				while (resultSet.next()) {
				    for (int i = 1; i <= columnsNumber; i++) {
				        if (i > 1) retVal +=(",  ");
				        retVal += resultSet.getString(i) + " " + rsmd.getColumnName(i);
				    }
				    retVal += "\n";
				    
				}
				resultSet.absolute(0);
				fileOutput += " " + retVal;
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				oFile.write(fileOutput.getBytes());
			}
			oFile.close();
		} catch (Exception e) {
			logger.info("query interceptor" + e);
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // if file already exists will do nothing 
	}
}
