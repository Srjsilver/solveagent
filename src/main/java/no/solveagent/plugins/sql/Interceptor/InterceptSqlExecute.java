package no.solveagent.plugins.sql.Interceptor;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.sql.PreparedStatement;
import java.time.LocalDateTime;
import net.bytebuddy.asm.Advice;
import no.solveagent.plugins.sql.JdbcMonitor;
import no.solveagent.plugins.sql.ThreadContext;

public class InterceptSqlExecute {

	
	@Advice.OnMethodEnter
	public static void before(
			@Advice.This Object obj,
			@Advice.Origin Method method) {
		
		ThreadContext threadContext = new ThreadContext();
		threadContext.setBeforeTimer(System.currentTimeMillis());
		threadContext.setMonitoredObject(obj);
		threadContext.setRunningThread(Thread.currentThread().getId());
		ThreadContext elem = JdbcMonitor.get(threadContext);
		if(elem == null) {
			JdbcMonitor.add(threadContext);
		}else {
			elem.setBeforeTimer(System.currentTimeMillis());
		}
	}
	
	@Advice.OnMethodExit
	public static void after(
			@Advice.This Object obj,
			@Advice.Origin Method method,
			@Advice.Return boolean returnValue) {
		Long endTime = System.currentTimeMillis();
		ThreadContext context = JdbcMonitor.get(obj, Thread.currentThread().getId());
		Long ms = endTime - context.getBeforeTimer();
		File yourFile = new File("Sql.txt");
		try {
			yourFile.createNewFile();
			String fileOutput = LocalDateTime.now().toString();
			FileOutputStream oFile = new FileOutputStream(yourFile, true);
			fileOutput += " execute took: " + ms + "ms and returned with: " + returnValue + "\n ";
			oFile.write(fileOutput.getBytes());
			oFile.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
