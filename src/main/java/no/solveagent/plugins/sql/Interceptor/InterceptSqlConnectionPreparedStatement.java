package no.solveagent.plugins.sql.Interceptor;
import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Method;
import java.time.LocalDateTime;

import net.bytebuddy.asm.Advice;
import no.solveagent.plugins.sql.JdbcMonitor;
import no.solveagent.plugins.sql.ThreadContext;


public class InterceptSqlConnectionPreparedStatement {

	@Advice.OnMethodEnter
	public static void interceptBefore(
			@Advice.This Object obj,
			@Advice.Origin Method method,
			@Advice.AllArguments Object[] allArguments)
	{
		ThreadContext threadContext = new ThreadContext();
		threadContext.setBeforeTimer(System.currentTimeMillis());
		threadContext.setMonitoredObject(obj.toString());
		threadContext.setRunningThread(Thread.currentThread().getId());
		ThreadContext elem = JdbcMonitor.get(threadContext);
		if(elem == null) {
			JdbcMonitor.add(threadContext);
		}else {
			elem.setBeforeTimer(System.currentTimeMillis());
		}
	}

	@Advice.OnMethodExit
	public static void interceptAfter(
			@Advice.This Object obj,
			@Advice.AllArguments Object[] allArguments,
			@Advice.Return Object returnObject,
			@Advice.Origin Method method) {

		Long endTime = System.currentTimeMillis();
		ThreadContext context = JdbcMonitor.get(obj.toString(), Thread.currentThread().getId());

		Long ms = endTime - context.getBeforeTimer();
		File yourFile = new File("Sql.txt");
		try {
			yourFile.createNewFile();
			String fileOutput = LocalDateTime.now().toString() + " \n ";
			FileOutputStream oFile = new FileOutputStream(yourFile, true);
			Method getMetaData = method.getDeclaringClass().getMethod("getMetaData");
			Object databaseMetaData = getMetaData.invoke(obj);
			Method getUrl = databaseMetaData.getClass().getMethod("getURL");
			String databaseUrl = (String) getUrl.invoke(databaseMetaData);
			fileOutput += databaseUrl + " ";
			if(allArguments != null) {
				fileOutput += " Sql: " + allArguments[0].toString() + " \n ";
			}
			if(returnObject != null) {
				fileOutput += " ReturnValue: " + returnObject.toString() + " \n ";
			}
			if(ms != null) {
				fileOutput += " method execution time: " + ms + " ms \n ";
			}
			fileOutput += " \n";
			oFile.write(fileOutput.getBytes());
			oFile.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}




}
