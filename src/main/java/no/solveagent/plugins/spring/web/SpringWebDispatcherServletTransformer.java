package no.solveagent.plugins.spring.web;

import net.bytebuddy.asm.Advice;
import net.bytebuddy.asm.AsmVisitorWrapper.ForDeclaredMethods;
import net.bytebuddy.description.type.TypeDescription;
import net.bytebuddy.matcher.ElementMatcher;
import net.bytebuddy.matcher.ElementMatchers;
import no.solveagent.core.agent.tansformer.SolveAgentTransformer;
import no.solveagent.plugins.spring.web.interceptor.DoDispatchInterceptor;

public class SpringWebDispatcherServletTransformer extends SolveAgentTransformer{

	private static final String dispatcherServlet = "org.springframework.web.servlet.DispatcherServlet";
	private static final String doDispatch = "doDispatch";
	
	@Override
	public ForDeclaredMethods getAdvice() {
		return Advice.to(DoDispatchInterceptor.class).on(ElementMatchers.named(doDispatch));
	}

	public ElementMatcher<? super TypeDescription> getType() {
		return ElementMatchers.named(dispatcherServlet);
	}
}
