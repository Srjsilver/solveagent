package no.solveagent.plugins.spring.web.monitor;

public class RequestAndResponseContext {
	
	private RequestContext req = null;
	private ResponseContext res = null;
	private Long thread = null;
	
	public RequestContext getReq() {
		return req;
	}
	public RequestAndResponseContext setReq(RequestContext req) {
		this.req = req;
		return this;
	}
	public ResponseContext getRes() {
		return res;
	}
	public RequestAndResponseContext setRes(ResponseContext res) {
		this.res = res;
		return this;
	}
	public Long getThread() {
		return thread;
	}
	public RequestAndResponseContext setThread(Long thread) {
		this.thread = thread;
		return this;
	}
	public static RequestAndResponseContext create() {
		return new RequestAndResponseContext();
	}
}
