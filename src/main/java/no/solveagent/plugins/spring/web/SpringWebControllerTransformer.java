package no.solveagent.plugins.spring.web;

import net.bytebuddy.asm.Advice;
import net.bytebuddy.asm.AsmVisitorWrapper.ForDeclaredMethods;
import net.bytebuddy.description.type.TypeDescription;
import net.bytebuddy.matcher.ElementMatcher;
import net.bytebuddy.matcher.ElementMatchers;
import no.solveagent.core.agent.tansformer.SolveAgentTransformer;
import no.solveagent.plugins.spring.web.interceptor.ControllerInterceptor;

public class SpringWebControllerTransformer extends SolveAgentTransformer{

	private static final String restController = "org.springframework.web.bind.annotation.RestController";
	private static final String controller = "org.springframework.stereotype.Controller";
	private static final String requestMapping = "org.springframework.web.bind.annotation.RequestMapping";
	
	@Override
	public ForDeclaredMethods getAdvice() {
		
		return Advice.to(ControllerInterceptor.class).on(ElementMatchers.isAnnotatedWith(ElementMatchers.named(requestMapping)));
	}

	public ElementMatcher<? super TypeDescription> getType() {
		return ElementMatchers.isAnnotatedWith(ElementMatchers.named(restController))
				.or(ElementMatchers.isAnnotatedWith(ElementMatchers.named(controller)));
	}

}
