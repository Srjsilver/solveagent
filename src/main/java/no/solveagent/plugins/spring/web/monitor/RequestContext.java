package no.solveagent.plugins.spring.web.monitor;

public class RequestContext {

	private String requestAsString = null;
	private String ms = null;

	public String getRequestAsString() {
		return requestAsString;
	}

	public RequestContext setRequestAsString(String requestAsString) {
		this.requestAsString = requestAsString;
		return this;
	}

	public static RequestContext create() {
		return new RequestContext();
	}

	public RequestContext setMs(String ms) {
		this.ms = ms;
		return this;
	}

	public String getMs() {
		return ms;
	}

}
