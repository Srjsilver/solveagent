package no.solveagent.plugins.spring.web.monitor;

public class ResponseContext {

	private Integer statusCode = null;
	private Object responseObject = null;
	
	public Integer getStatusCode() {
		return statusCode;
	}
	public ResponseContext setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
		return this;
	}
	public Object getResponseObject() {
		return responseObject;
	}
	public ResponseContext setResponseObject(Object responseObject) {
		this.responseObject = responseObject;
		return this;
	}
	public static ResponseContext create() {
		return new ResponseContext();
	}
}
