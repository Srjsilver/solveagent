package no.solveagent.plugins.spring.web.interceptor;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.LocalDateTime;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.bytebuddy.asm.Advice;
import no.solveagent.plugins.spring.web.monitor.RequestAndResponseContext;
import no.solveagent.plugins.spring.web.monitor.SpringRequestAndResponseWrapper;

public class DoDispatchInterceptor {

	@Advice.OnMethodEnter
	public static void interceptBefore(@Advice.This Object thisObject, @Advice.AllArguments Object[] allArguments,
			@Advice.Origin Method method) {

		if (allArguments[0].getClass().getSimpleName().contains("Request")) {
			Class<?> request = allArguments[0].getClass();
			try {
			Method getRemoteaddr = request.getMethod("getRemoteAddr");
			Method getQueryString = request.getMethod("getQueryString");
			Method url = request.getMethod("getRequestURL");
			String ipAddress = (String) getRemoteaddr.invoke(allArguments[0]) + ";";
			String queryString = (String) getQueryString.invoke(allArguments[0]);
			StringBuffer urll = (StringBuffer) url.invoke(allArguments[0]);
			String requestAsString = ipAddress +  urll;
			if(queryString != null) {
				requestAsString += '?';
				requestAsString += queryString;
			}
			SpringRequestAndResponseWrapper.addRequestContext(Thread.currentThread().getId(), requestAsString);
			} catch(Exception e) {
				// do nothing
			}
		}
	}

	@Advice.OnMethodExit
	public static void intercept(@Advice.This Object thisObject, @Advice.AllArguments Object[] allArguments,
			@Advice.Origin Method method) {
		//read full request and response
		
		Object response = allArguments[1];
		Class<?> responseClass = response.getClass();
		Method getStatus;
		Integer status = -1;
		try {
			getStatus = responseClass.getMethod("getStatus");
			status = (Integer) getStatus.invoke(response);
			
		} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			// nothing
		}
		RequestAndResponseContext context = SpringRequestAndResponseWrapper.getContext(Thread.currentThread().getId());
		try {
			File fileName = new File("Spring_Request_Response.txt");
			fileName.createNewFile();
			String fileOutput = LocalDateTime.now().toString() + ";";
			FileOutputStream oFile = new FileOutputStream(fileName, true);
			fileOutput += context.getReq().getMs() + ";";
			fileOutput += context.getReq().getRequestAsString() + ";";
			fileOutput += ""+status + ";";
			ObjectMapper objectMapper = new ObjectMapper();
			fileOutput += "" + objectMapper.writeValueAsString(context.getRes().getResponseObject()) + '\n';
			oFile.write(fileOutput.getBytes());
			oFile.close();
		} catch (Exception e) {
			// nothing
		}
		
		//should not grow, clean up
		SpringRequestAndResponseWrapper.destroyRequestAndResponseContext(Thread.currentThread().getId());
	}
}
