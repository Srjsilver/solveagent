package no.solveagent.plugins.spring.web.interceptor;

import java.lang.reflect.Method;

import net.bytebuddy.asm.Advice;
import no.solveagent.plugins.spring.web.monitor.SpringRequestAndResponseWrapper;

public class ControllerInterceptor {

	@Advice.OnMethodExit
	public static void intercept(@Advice.This Object thisObject, @Advice.Return Object returnValue,
			@Advice.AllArguments Object[] allArguments, @Advice.Origin Method method) {

		SpringRequestAndResponseWrapper
				.appendResponseContext(Thread.currentThread().getId(), returnValue);
	}
}
