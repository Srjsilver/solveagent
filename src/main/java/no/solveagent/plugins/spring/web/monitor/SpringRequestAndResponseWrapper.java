package no.solveagent.plugins.spring.web.monitor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SpringRequestAndResponseWrapper {
	
	private static List<RequestAndResponseContext> listOfRequestAndResponse;
	
	static {
		listOfRequestAndResponse = new ArrayList<>();
	}
	
	public synchronized static void addRequestContext(Long thread, String requestContextAsString) {
		RequestContext reqContext = RequestContext.create()
				.setRequestAsString(requestContextAsString);
		RequestAndResponseContext context = RequestAndResponseContext.create()
				.setReq(reqContext)
				.setThread(thread);
		listOfRequestAndResponse.add(context);
	}
	public synchronized static RequestAndResponseContext appendResponseContext(Long thread, Object responseObject) {
		ResponseContext resContext = ResponseContext.create()
				.setResponseObject(responseObject);
		for(RequestAndResponseContext elem : listOfRequestAndResponse) {
			if(elem.getThread() == thread) {
				elem.setRes(resContext);
				return elem;
			}
		}
		return null;
	}

	
	public synchronized static void destroyRequestAndResponseContext(Long thread) {
		for(Iterator<RequestAndResponseContext> it = listOfRequestAndResponse.iterator(); it.hasNext();) {
			RequestAndResponseContext elem = it.next();
			if(thread == elem.getThread()) {
				it.remove();
				return;
			}
		}
	}
	public static Integer size() {
		return listOfRequestAndResponse.size();
	}
	public synchronized static RequestAndResponseContext getContext(Long thread) {
		for(RequestAndResponseContext elem : listOfRequestAndResponse) {
			if(elem.getThread() == thread) {
				return elem;
			}
		}
		return null;
	}
	
}
