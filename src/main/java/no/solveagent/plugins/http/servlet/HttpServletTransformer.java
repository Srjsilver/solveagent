package no.solveagent.plugins.http.servlet;

import net.bytebuddy.asm.Advice;
import net.bytebuddy.asm.AsmVisitorWrapper.ForDeclaredMethods;
import net.bytebuddy.description.type.TypeDescription;
import net.bytebuddy.matcher.ElementMatcher;
import net.bytebuddy.matcher.ElementMatchers;
import no.solveagent.core.agent.tansformer.SolveAgentTransformer;

public class HttpServletTransformer extends SolveAgentTransformer{

	private static final String httpServlet = "javax.servlet.http.HttpServlet";
	private static final String service = "service";
	
	@Override
	public ForDeclaredMethods getAdvice() {
		return Advice.to(ServiceInterceptor.class).on(ElementMatchers.named(service));
	}

	@Override
	public ElementMatcher<? super TypeDescription> getType() {
		return ElementMatchers.named(httpServlet);
	}

}
