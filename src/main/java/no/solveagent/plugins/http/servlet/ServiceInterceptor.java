package no.solveagent.plugins.http.servlet;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Method;
import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.bytebuddy.asm.Advice;

public class ServiceInterceptor {

	@Advice.OnMethodExit
	public static void intercept(
			@Advice.AllArguments Object[] allArguments,
			@Advice.Origin Method method) {
		
		
		Logger logger = LoggerFactory.getLogger(method.getDeclaringClass());
		logger.info("Method {} of class {} called", method.getName(), method
				.getDeclaringClass().getSimpleName());

			Class<?> request = allArguments[0].getClass();
			String fileOutput = LocalDateTime.now().toString();
			
			try {
				
				//get client ip info
				Method getRemoteaddr = request.getMethod("getRemoteAddr");
				Method getQueryString = request.getMethod("getQueryString");
				Method url = request.getMethod("getRequestURL");
				String ipAddress = (String) getRemoteaddr.invoke(allArguments[0]);
				String queryString = (String) getQueryString.invoke(allArguments[0]);
				StringBuffer urll = (StringBuffer) url.invoke(allArguments[0]);

				if(queryString == null) {
					fileOutput +=  " caller: " + ipAddress + " did request : " + urll + " ";
				}else {
					fileOutput += " caller: " + ipAddress + " did request : " +  urll.append('?').append(queryString).toString() + " ";
				}
				
			}catch(Exception e) {
				
			}
			
			Class<?> response = allArguments[1].getClass();
			try {
				Method getStatuscode = response.getMethod("getStatus");
				Integer statusCode = (Integer) getStatuscode.invoke(allArguments[1]);
				fileOutput += " status code for request is: " + statusCode + " \n";
				File yourFile = new File("Service.txt");
				yourFile.createNewFile(); // if file already exists will do nothing 
				FileOutputStream oFile = new FileOutputStream(yourFile, true);
				oFile.write(fileOutput.getBytes());
				oFile.close();
			}catch(Exception e) {
				
			}
			
	}
	
}
