package no.solveagent.integrations.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import net.bytebuddy.agent.ByteBuddyAgent;
import no.solveagent.app.SolveAgent;

public class SqlTestClass {

	Connection con = null;
	DataSource ds = dataSource();

	@Before
	public void initiateDB() {
		try {
			con = ds.getConnection();
			Statement s = con.createStatement();
			s.executeUpdate("CREATE TABLE PERSON (ID INT PRIMARY KEY, FIRSTNAME VARCHAR(64))");
			s.executeUpdate("INSERT INTO PERSON (ID, FIRSTNAME) VALUES(1, 'PER')");
			s.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void JdbcMonitorTest() {
		SolveAgent.premain("", ByteBuddyAgent.install());
		try {
			PreparedStatement ps = con.prepareStatement("select * from person where id = ? AND firstname = ?");
			ps.setObject(1, 1);
			ps.setInt(1, 1);
			ps.setString(2, "PER");
			ps.execute();
			ps.close();
			
			ps = con.prepareStatement("select * from person where id = ?");
			ps.setObject(1, 1);
			ps.setInt(1, 1);
			ps.executeQuery();
			ps.close();
			con.commit();
			con.close();
			
		} catch (SQLException e) {
			e.printStackTrace();
			// TODO Auto-generated catch block
		}
	}
	@Test
	public void testSubStrings() {
		String l = "setArray;setBigDecimal"
                + ";setBoolean;setByte;setDate;setDouble;setFloat;setInt;setLong;setNString"
                + ";setRef;setRowId;setShort;setString;setTime;setTimestamp;setURL";
		String[] strngs = l.split(";");
		for(String s : strngs) {
			System.out.print(s + " ");
		}
	}

	private DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("org.h2.Driver");
		dataSource.setUrl("jdbc:h2:mem:db;DB_CLOSE_DELAY=-1;DATABASE_TO_UPPER=false");
		dataSource.setUsername("sa");
		dataSource.setPassword("sa");
		return dataSource;
	}
}
